### Taygete CMS  
[![license](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat)](https://gitlab.com/huaiping/taygete/blob/master/LICENSE) [![Build Status](https://travis-ci.com/huaiping/taygete.svg?branch=master)](https://travis-ci.com/huaiping/taygete)  
Open Source CMS. Just for practice.

### Requirements
.NET Core 2.2+ [https://dotnet.microsoft.com](https://dotnet.microsoft.com)  
MySQL 5.5+ [https://www.mysql.com](https://www.mysql.com)

### Features
HTML 5 + .NET Core 2.2.1 + Bootstrap 4.1.3

### Installation
```
$ git clone https://github.com/huaiping/taygete.git
$ cd taygete
$ dotnet run
```

### Demo
[https://www.huaiping.net](https://huaiping.net/v2/)

### License
Licensed under the [MIT License](https://gitlab.com/huaiping/taygete/blob/master/LICENSE).
